import PropTypes from "prop-types";
import TodoItem from "./TodoItem";

function TodoList(props) {
  const { todos } = props;

  // Variable pour stocker le tableau filtré (seulement les tâche à completer)
  const toCompleteToDo = todos.filter(item =>
  item.completed === false
);

  // Variable pour stocker le nouveau tableau .map avec le composant ToDoItem
  const listToDo = toCompleteToDo.map(item =>
    <TodoItem
    key= {item.id}
    title={item.title}
    completed={item.completed} />
  )
  return (
    <div>
      <h1>My todo list ({todos.length} items):</h1>
      <ul style={{ listStyle: 'none' }}>{listToDo}</ul>
    </div>
  );
}

TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
};

export default TodoList;

